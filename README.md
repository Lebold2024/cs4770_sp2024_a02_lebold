## Jacob R Lebold
Bachelor of Science in Computer Science, No specialization

## Q/A

**Defining Terms**

**Agent:**

An agent is anything that can perceive its environment through sensors, and is able to make actions based on what it percieves.

**Environment:**

An environment to an AI is the surroundings or area where the AI agent functions in. It is a diverse field that contains many different identifiers for many different types of environments an agent may function in. 

**Sensor:**

Sensors are what allows agents to actually perceive the environment that it is in. They measure different properties of the environment and the agent uses that information. 

**Actuator:**

Actuators are tools that the agent makes use of to carry out different tasks. For example, an autonomous vehicle's actuators would be accelerator, brakes, horn, etc. 

**Percept**

Percept's are the information the agent is currently "seeing" at a point in time. It will use this information it is currently seeing to decide what action to take

**Agent Function**

"The agent function maps from percept histories to actions". It is a function that defines an agents decision making. It will take a percept sequence and convert them to different actions it can/will take. 

**Agent Program**

The software of the agent, runs on the physical arcitecture of the agent to produce the Agent Function. 

##Defining Agents

**Microwave Oven:**

Sensors: Temperature guage, to tell the temperature inside the oven. Door sensor, stopping the microwave from use when the door is currently open

Actuators: Turntable, buttons for users to set the time they want the food cooked for

Environment: Enclosed space, where the food will go. 

**Chess Program**

Sensors: Mouse and keyboard that the user can use to move their pieces on the board. 

Actuators: User Interface, the representation of a game board on the screen. 

Environment: The chessboard and the position of the pieces on that board. The rules of chess also play into the environment, where pieces can currently go and where they cannot go. 

**Autonomous Supply Delivery Plane**

Sensors: GPS to provide information of the route it will take. Weather sensors to detect changes in the weather. Cameras for identifying different aspects it will need to see, like where to land.

Actuators: Engine controls, everything that goes into making a plane actually fly. 

Environment: Airspace, it is flying in the sky. Landing sites and takeoff sites. Location of the deliveries. 

## True or False

**An agent that senses only partial information about the state cannot be perfectly rational**

False, while the agent may not have all the information about the state, it is still capable of choosing the action that will maximize the expected value of the performance measure. So it will be perfectly rational, but it might not be always correct, since it only has partial information. 


**There exists a task environment in which every agent is rational**

True, if within the state, all actions taken by the agents will have the exact same reward, it will not matter what action each agent performs. In the real world however, this is almost impossible, as there is too much uncertanity and incomplete information for every agent to be perfectly rational. 

**The input to an agent program is the same as the input to the agent function**

False, the agent program takes in the current percept only, while the agent function takes in the entire percept histories up to the point of a decision (if designed to). The agent function has more data to work with, while the agent program only takes in a single piece of data at a time. 

## **What does PEAS stand for?**

PEAS is an acronym used to describe different aspects of a task environment. This involves describing **P**erformance Measure, **E**nvironment, **A**ctuators, and **S**ensors

## PEAS description of different tasks

**Playing Soccer:**

Performance Measure: Goals scored, teammates you have assisted, energy you have used and the amount of energy you have left

Environment: Perfect Grass soccer field, turf field, muddy field, street. (Soccer can be played in a lot of places).

Actuators: Leg muscles, feet, arm muscles and hands (for goalies), lungs. 

Sensors: Eyesight, physical touch (motivation and mood? not sure if that would count as a sensor, but it is something that you feel when playing a sport.)

**Performing high jumps:**

Performance Measure: Height of the bar, height of your jump, how many attempts you have taken, run-up point (where you started the run from), your landing. 

Environment: Cross-country field, typically on solid/smooth pavement, stadium environment (professional)

Actuators: Leg muscles (calves, hamstrings, quads, etc.), core muscles, shoulder and arm muscles can help with getting higher jumps

Sensors: Eyesight, Physical touch (soreness), (motivation and mood?).